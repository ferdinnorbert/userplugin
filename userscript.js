jQuery(document).ready(function($) {

    document.body.innerHTML = "";
    var head1 = "<link rel='stylesheet' type='text/css' href='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css'>";
    var head2 = "<script src='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js'></script>";

    var api_url = "https://jsonplaceholder.typicode.com/users";
    var modal = '';
    // $.getJSON(api_url,function(data){

    // });

    var prevReq = $.Deferred().resolve().promise();

    function getData(api_url) {
        var newReq = $.ajax({
            type: "GET",
            url: api_url,
            contentType: "application/json",
            dataType: 'json',
            timeout: 2000
        }).then(function(data){
            // if successful, set previous request to this request
            prevReq = newReq;
            return data;
        },function(){
            // if it fails, return the previous request
            return prevReq;
        });
        return newReq;
    }

    getData(api_url).done(function(data){
      var table = '';
      table += '<table class="table table-hover">';
      table += '<tr>';
      table += '<th>ID</th>';
      table += '<th>Name</th>';
      table += '<th>Username</th>';
      table += '</tr>';

      var counter = 0;
      $.each(data, function(key,value){

        table += '<tr>';
        table += '<td><a href="#" data-toggle="modal" data-target="#detail-'+value.id+'">'+value.id+'</a></td>';
        table += '<td><a href="#" data-toggle="modal" data-target="#detail-'+value.id+'">'+value.name+'</a></td>';
        table += '<td><a href="#" data-toggle="modal" data-target="#detail-'+value.id+'">'+value.username+'</a></td>';
        table += '</tr>';
      });
      table += '</table>';
      $.each(data, function(key,value){
        //Create the modal
        modal += '<div class="modal fade" id="detail-'+value.id+'">'
        modal += '<div class="modal-dialog">';
        modal += '<div class="modal-content">';

        //Modal Header
        modal += '<div class="modal-header">';
        modal += '<h4 class="modal-title">Details</h4>';
        modal += '<button type="button" class="close" data-dismiss="modal">&times;</button>';
        modal += '</div>';

        //Modal Body
        modal += '<div class="modal-body">';
        modal += '<strong>Name : </strong>'+value.name+'<br/>';
        modal += '<strong>Username : </strong>'+value.username+'<br/>';
        modal += '<strong>Email : </strong>'+value.email+'<br/>';
        modal += '<strong>Address : </strong>'+value.address.street+', '+value.address.suite+', '+value.address.city+', '+value.address.zipcode+'<br/>';
        modal += '<strong>Phone : </strong>'+value.phone+"<br/>";
        modal += '<strong>Website : </strong>'+value.website+"<br/>";
        modal += '<strong>Location : </strong><a href="https://www.google.com/maps/search/?api=1&query='+value.address.geo.lat+','+value.address.geo.lng+'">Click Here</a><br/>';
        modal += '<strong>Company Details : </strong><br/>';
        modal += '<ul class="list-group">';
        modal += '<li class="list-group-item">Name : '+value.company.name+'</li>';
        modal += '<li class="list-group-item">Motto : '+value.company.catchPhrase+'</li>';
        modal += '<li class="list-group-item">Business Type : '+value.company.bs+'</li>';
        modal += '</ul>';
        modal += '</div>';

        //Modal Footer
        modal += '<div class="modal-footer">';
        modal += '<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>';
        modal += '</div>';

        modal += '</div>';
        modal += '</div>';
        modal += '</div>';
        $('body').append(modal);
      });
      $('head').append(head1);
      $('head').append(head2);
      $('body').append(table);
    })
});
