<?php
/*
  Plugin Name: User Plugin
  Plugin URI:
  Description: User Plugin to parse details in json format and display on the page
  Author: Ferdin Norbert
  Author URI: https://github.com/Ferdin
  Version: 0.1
*/

register_activation_hook(__FILE__, function () {
    global $wpdb;

    $pageTitle = 'User list';
    $pageName = 'userlist';

  // the menu entry...
    delete_option("my_plugin_page_title");
    add_option("my_plugin_page_title", $pageTitle, '', 'yes');
  // the slug...
    delete_option("my_plugin_page_name");
    add_option("my_plugin_page_name", $pageName, '', 'yes');
  // the id...
    delete_option("my_plugin_page_id");
    add_option("my_plugin_page_id", '0', '', 'yes');

    $thePage = get_page_by_title($pageTitle);

    if (!$thePage) {
        // Create post object
        $postObject = [];
        $postObject['post_title'] = $pageTitle;
        $postObject['post_content'] = "[userplugin]";
        $postObject['post_status'] = 'publish';
        $postObject['post_type'] = 'page';
        $postObject['comment_status'] = 'closed';
        $postObject['ping_status'] = 'closed';
        $postObject['post_category'] = [1]; // the default 'Uncatrgorised'
        // Insert the post into the database
        $pageID = wp_insert_post($postObject);
    } else {
        // the plugin may have been previously active and the page may just be trashed...
        $pageID = $thePage->ID;
        //make sure the page is not trashed...
        $thePage->post_status = 'publish';
        $pageID = wp_update_post($thePage);
    }
    delete_option('my_plugin_page_id');
    add_option('my_plugin_page_id', $pageID);
});

    /* Runs on plugin deactivation */
register_deactivation_hook(__FILE__, function () {
    global $wpdb;
    $pageTitle = get_option("my_plugin_page_title");
    $pageName = get_option("my_plugin_page_name");
  //  the id of our page...
    $pageID = get_option('my_plugin_page_id');
    if ($pageID) {
        wp_delete_post($pageID); // this will trash, not delete
    }
    delete_option("my_plugin_page_title");
    delete_option("my_plugin_page_name");
    delete_option("my_plugin_page_id");
}) ;

add_shortcode('userplugin', function () {
    if (is_page('user-list')) {
        wp_enqueue_script('script', plugins_url('/userscript.js', __FILE__), ['jquery']);
    }
});
