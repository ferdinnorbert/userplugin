# Userplugin

Userplugin is a WordPress plugin use to display the details of users.

## Installation

Clone the plugin to wordpress->wp-content->plugins folder.
Go to the Terminal and locate this downloaded project folder and run composer update. It will install all the dependencies.

```bash
composer update
```

Now when you go to the admin panel of your Wordpress site, you can see under the *Plugins* section our installed plugin called *Userplugin*. Click to activate.

## Usage

This plugin will generate a page under the *page* section called *User list*. Be careful **don't trash that page**. If, by any chance, you delete it, Just go to the plugin page and re-activate it again will fix the issue.  
In order to **open the page**, go to your domain/user-list
```bash
domain-name/user-list
```
**For example**
```bash
wordpress.com/user-list
```
From this endpoint, you can access the plugin generated page.

**Below is how your page looks like after navigating to the user-list**
![Screen Shot 2020-04-04 at 1 39 13 PM](https://user-images.githubusercontent.com/6976190/78457888-8ef99e00-767b-11ea-9165-0ac50fde4424.png)


Inside that page, you can find a list of users in a table format. There are **3 columns**, namely - **ID, Name** and **Username**.  
All these ids, names and username under respective columns are **links**.  
When you **click** on any of these **links**, a modal will appear. For example, like the image below.
![Screen Shot 2020-04-04 at 1 39 26 PM](https://user-images.githubusercontent.com/6976190/78457944-e992fa00-767b-11ea-92e3-6a240346938c.png)

## Rationale

- When the plugin is activated, a page named *User-List* is created through *register_activation_hook()*. This page has a custom shortcode *[userplugin]* which customizes the plugin page overriding the current page styles.
- The plugin checks if the user is on the page 'user-list' in order to execute the *jQuery* script.
- jQuery script dynamically adds the relevant stylings such as emptying the body, dynamically creating the table and adding the headers for the styling ie getting the *CDN* of *Bootstrap* which will automatically cache.
- Using *AJAX*, we asynchronously retrieve the JSON data. In order to reduce new HTTP requests, this script maintains the first request received. That way we can maintain the cache for the JSON data.
- Afterwards, we manipulate the retrieved JSON and dynamically build a table using jQuery alongside with Bootstrap styling.
- Each element in the table is a link. When you click on any link, corresponding modal will popup, showing the details of the particular user.
- In order to prevent creating new pages at each 'activate' clicks of the Plugin. In the index.php, it has a hook called register_deactivation_hook. When the user deactivates the plugin, it will remove the 'user-list' page.

## Tools Used

- **Atom** for the coding environment.
- **Wordpress** locally installed for testing the plugin.
- **Composer** for installing relevant packages.
- **inpsyde/php-coding-standards**, a package for coding standards, installed through composer.

## Programming

```bash
PHP, jQuery and Bootstrap
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.


## License
[MIT](https://choosealicense.com/licenses/mit/)